using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz_GeorgeT
{
    class Program
    {
        static void Main(string[] args)
        {
         
            for (int value = 1; value < 101; value++)
            {
               
                if (value % 3 == 0) 
                {
                    Console.WriteLine($" {value} Fizz    ");
                }

                if (value % 5 == 0)
                {
                    Console.WriteLine($" {value} Buzz    ");
                }

                else
                {
                    Console.WriteLine($" {value}     ");
                }
            }

            Console.ReadLine(); // Pause
        }
    }
}
